[PHP]
engine = On
short_open_tag = On
precision = 14
output_buffering = 4096
zlib.output_compression = Off
implicit_flush = Off
unserialize_callback_func =
serialize_precision = 17
open_basedir = none

disable_functions = pcntl_alarm,pcntl_fork,pcntl_waitpid,pcntl_wait,pcntl_wifexited,pcntl_wifstopped,pcntl_wifsignaled,pcntl_wexitstatus,pcntl_wtermsig,pcntl_wstopsig,pcntl_signal,pcntl_signal_dispatch,pcntl_get_last_error,pcntl_strerror,pcntl_sigprocmask,pcntl_sigwaitinfo,pcntl_sigtimedwait,pcntl_exec,pcntl_getpriority,pcntl_setpriority, apache_get_modules, apache_get_version, apache_getenv, apache_note, apache_setenv, dl, escapeshellcmd, exec, highlight_file, ini_alter, ini_restore, openlog, parse_ini_file, passthru, pcntl_exec, popen, proc_open, proc_nice, proc_close, proc_get_status, proc_terminate, putenv, shell_exec, show_source, system, virtual, chgrp, chmod, chown, syslog, get_loaded_extensions

disable_classes = splfileobject

date.timezone = Europe/Zurich
zend.enable_gc = On
expose_php = Off
max_execution_time = 120
max_input_time = 120
memory_limit = 64M
error_reporting = E_ALL & ~E_DEPRECATED & ~E_NOTICE & ~E_STRICT
display_errors = Off
display_startup_errors = Off
log_errors = On
log_errors_max_len = 1024
ignore_repeated_errors = Off
ignore_repeated_source = Off
report_memleaks = On
track_errors = Off
html_errors = Off
error_log = /home/sites/SITE_NAME/pub/log/php-fpm-errors.log
variables_order = "GPCS"
request_order = "GP"
register_argc_argv = Off
auto_globals_jit = On
post_max_size = 1024M
auto_prepend_file =
auto_append_file =
default_mimetype = "text/html"
doc_root =
user_dir =
enable_dl = Off
cgi.fix_pathinfo=0
file_uploads = On
upload_tmp_dir = /home/sites/SITE_NAME/pub/tmp
upload_max_filesize = 1024M
max_file_uploads = 32
allow_url_fopen = Off
allow_url_include = Off
default_socket_timeout = 120
realpath_cache_size = 64k
realpath_cache_ttl = 3600

[Pdo_mysql]
pdo_mysql.cache_size = 2000
pdo_mysql.default_socket=

[mail function]
SMTP = localhost
smtp_port = 25
mail.add_x_header = On

[SQL]
sql.safe_mode = Off

[ODBC]
odbc.allow_persistent = On
odbc.check_persistent = On
odbc.max_persistent = -1
odbc.max_links = -1
odbc.defaultlrl = 4096
odbc.defaultbinmode = 1

[MySQL]
mysql.allow_local_infile = On
mysql.allow_persistent = On
mysql.cache_size = 2000
mysql.max_persistent = -1
mysql.max_links = -1
mysql.default_port =
mysql.default_socket =
mysql.default_host =
mysql.default_user =
mysql.default_password =
mysql.connect_timeout = 360
mysql.trace_mode = Off

[MySQLi]
mysqli.max_persistent = -1
mysqli.allow_persistent = On
mysqli.max_links = -1
mysqli.cache_size = 2000
mysqli.default_port = 3306
mysqli.default_socket =
mysqli.default_host =
mysqli.default_user =
mysqli.default_pw =
mysqli.reconnect = Off

[mysqlnd]
mysqlnd.collect_statistics = On
mysqlnd.collect_memory_statistics = Off

[PostgreSQL]
pgsql.allow_persistent = On
pgsql.auto_reset_persistent = Off
pgsql.max_persistent = -1
pgsql.max_links = -1
pgsql.ignore_notice = 0
pgsql.log_notice = 0

[bcmath]
bcmath.scale = 0

[Session]
session.save_handler = files
session.save_path = "/home/sites/SITE_NAME/pub/sessions"
session.use_cookies = 1
;session.cookie_secure =
session.use_only_cookies = 1
session.name = PHPSESSID
session.auto_start = 0
session.cookie_lifetime = 0
session.cookie_path = /
session.cookie_domain =
session.cookie_httponly =
session.serialize_handler = php
session.gc_probability = 1
session.gc_divisor = 100
session.gc_maxlifetime = 1440
session.bug_compat_42 = Off
session.bug_compat_warn = Off
session.referer_check =
session.cache_limiter = nocache
session.cache_expire = 180
session.use_trans_sid = 0
session.hash_function = 0
session.hash_bits_per_character = 5
url_rewriter.tags = "a=href,area=href,frame=src,input=src,form=fakeentry"

[Tidy]
tidy.clean_output = Off

[soap]
soap.wsdl_cache_enabled=1
soap.wsdl_cache_dir="/tmp"
soap.wsdl_cache_ttl=86400
soap.wsdl_cache_limit = 5

[ldap]
ldap.max_links = -1

[opcache]
zend_extension=opcache.so

; set to 1 to enable
opcache.enable = 1
opcache.enable_cli = 0

; This hack should only be enabled to work around "Cannot redeclare class" errors.
;opcache.dups_fix = 1

; The default is 64MB. You can use the function opcachegetstatus() to tell how much memory 
; opcache is consuming and if you need to increase the amount 
opcache.memory_consumption = 128MB

; Controls how many PHP files, at most, can be held in memory at once. 
; It's important that your project has LESS FILES than whatever you set this at. 
opcache.max_accelerated_files = 4000

; How often (in seconds) should the code cache expire and check if your code has changed. 
; 0 means it checks your PHP code every single request (which adds lots of stat syscalls). 
; Set it to 0 in your development environment. Production doesn't matter because of the next setting.
opcache.revalidate_freq = 60

; When this is enabled, PHP will check the file timestamp per your opcache.revalidate_freq value.
; When it's disabled, opcache.revaliate_freq is ignored and PHP files are NEVER checked for updated code. 
; So, if you modify your code, the changes won't actually run until you restart or reload PHP.
opcache.validate_timestamps = 1

; PHP uses a technique called string interning to improve performance, for example, if you have the 
; string "foobar" 1000 times in your code, internally PHP will store 1 immutable variable for this 
; string and just use a pointer to it for the other 999 times you use it.
opcache.interned_strings_buffer = 8

; What this actually does is provide a faster mechanism for calling the deconstructors in your code 
; at the end of a single request to speed up the response and recycle php workers so they're ready 
; for the next incoming request faster. 
opcache.fast_shutdown = 1

; If enabled, OPcache appends the current working directory to the script key, thereby eliminating 
; possible collisions between files with the same base name. Disabling this directive improves performance, 
; but may break existing applications.
opcache.use_cwd = 1

; If disabled, all documentation comments will be discarded from the opcode cache to reduce the size of 
; the optimised code. Disabling this configuration directive may break applications and frameworks that 
; rely on comment parsing for annotations, including Doctrine, Zend Framework 2 and PHPUnit.
opcache.save_comments = 1

 


