###
### Nginx Wordpress site with SSL
###

### FCGI-Cacheing: Block
#fastcgi_cache_path              /home/sites/SITE_NAME/cache
#                                levels=1:2
#                                keys_zone=fc_SITE_NAME:64M
#                                inactive=30h
#                                max_size=1024M;

### Redirect alias domains
server {
    server_name                 *.SITE_NAME;

    ### Use this for multiple alias domains
#    include                     /home/sites/SITE_NAME/conf/nginx-domain-aliases.conf;

    ssl_certificate             /home/sites/SITE_NAME/ssl/package.pem;
    ssl_certificate_key         /home/sites/SITE_NAME/ssl/private.key;
    listen                      *:80;
    listen                      *:443 ssl;
    http2                       on;
    error_log                   /home/sites/SITE_NAME/pub/log/nginx-error.log warn;
    return                      301 https://SITE_NAME$request_uri;
}

### Redirect all non-encrypted to encrypted
server {
    server_name                 SITE_NAME;
    listen                      *:80;
    error_log                   /home/sites/SITE_NAME/pub/log/error.log warn;
    return                      301 https://SITE_NAME$request_uri;
}

### Server block (aka. virtualhost)
server {
    server_name                 SITE_NAME;
    listen                      *:443 ssl;
    http2                       on;

    ### Uncomment to enable FCGI cache logging
#    access_log                  /home/sites/SITE_NAME/pub/log/nginx-access.log cache_log;
    error_log                   /home/sites/SITE_NAME/pub/log/nginx-error.log warn;
    root                        /home/sites/SITE_NAME/pub/httpdocs;
    index                       index.html index.php;

    ssl_certificate             /home/sites/SITE_NAME/ssl/package.pem;
    ssl_certificate_key         /home/sites/SITE_NAME/ssl/private.key;

    ### Uncomment to allow backend only from our IPs
#    if ($not-our-ips) {
#        rewrite                 ^/wp-login.php http://domain.tld? redirect;
#    }

    ### Block certain user agents
#    if ($http_user_agent ~ "SEOkicks-Robot|SISTRIX|seoscanners.net|MJ12bot|UptimeRobot") {
#        return 444;
#        break;
#    }

    ### Primary location, matches probably last
    location / {
#        auth_basic              "Restricted Area";
#        auth_basic_user_file    /home/sites/SITE_NAME/auth/htpasswd;

        ### SPECIAL: Allow cross domain ressources
#        add_header              Access-Control-Allow-Origin *;

        ### SPECIAL: Disallow search engines by force
#        add_header              X-Robots-Tag "noindex, nofollow, nosnippet, noarchive";



        ### WordPress Cachify Plugin integration
#        include                     /home/sites/SITE_NAME/conf/nginx/cachify.conf;




        try_files               $uri $uri/ /index.html /index.php?$args;
    }

    ### External file for SEO rewrites
    include                     /home/sites/SITE_NAME/conf/nginx-rewrites.conf;

    location = /favicon.ico {
        log_not_found           off;
    }

    location = /robots.txt {
        log_not_found           off;
    }



    ### WordPress Yoast Plugin integration
#    include                     /home/sites/SITE_NAME/conf/nginx/yoast-seo.conf;



    ### Add trailing slash to */wp-admin
    rewrite                     /wp-admin$ $scheme://$host$uri/ permanent;

    ### Prevent public access for Wordpress internals
    location ~* wp-admin/includes { deny all; }
    location ~* wp-includes/theme-compat/ { deny all; }
    location ~* wp-includes/js/tinymce/langs/.*\.php { deny all; }

    ### Prevent potentially-executable files in the uploads directory
    location ~* /uploads/.*.(html|htm|shtml|php)$ {
        types                   { }
        default_type            text/plain;
    }

    ### Disable direct access of any *.php in /wp_content/uploads folder
    location ~* /uploads/.+\.php$ {
        return 404;
    }

    ### Disallow access to important files
    location ~* (/\.|wp-config\.php|wp-env\.php|(liesmich|readme).*) {
        return 404;
    }



    ### Cache header and on-demand thumbnail generation variants (uncomment only one)

    ### Variant 1: Simple WordPress (no WP Performance Pack, no WebP Express)
    include                     /home/sites/SITE_NAME/conf/nginx/cache-headers-simple.conf;

    ### Variant 2: Extended WordPress (WP Performance Pack, no WebP Express)
#    include                     /home/sites/SITE_NAME/conf/nginx/cache-headers-extended.conf;

    ### Variant 3: Fully Fledged WordPress (WP Performance Pack, WebP Express)
#    include                     /home/sites/SITE_NAME/conf/nginx/cache-headers-full.conf;




    ### Prevent access to any files starting with a dot, like .htaccess or temp files
    location ~ /\. {
        log_not_found           off;
        deny                    all;
    }

    ### Prevent access to any files starting with a $ (usually temp files)
    location ~ ~$ {
        log_not_found           off;
        deny                    all;
    }

    ### Make CertBot work with webroot manual authentication with ACME challenge
    location ^~ /.well-known/acme-challenge/ {
        try_files               $uri $uri/ =404;
    }

    ### Allow access to web-hooks only for certain local requests
#    include                     /home/sites/SITE_NAME/conf/nginx/web-hooks.conf;




    ### Do not cache social media crawler
#    set $override_cache 0;
#    if ($http_user_agent ~* "facebookexternalhit|twitterbot|developers\.google\.com") {
#        set $override_cache 1;
#    }

    location ~ \.php$ {
        include                 fastcgi_params;
#        fastcgi_pass            localhost:PHPFPM_PORT;
        fastcgi_pass            unix:/home/sites/SITE_NAME/run/php-fpm.sock;
        fastcgi_temp_path       /home/sites/SITE_NAME/pub/tmp/fcgi 1 2;
        fastcgi_param           SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param           HTTPS on;
        fastcgi_param           REQUEST_METHOD $request_method;

        ### Uncomment to let webapp handle the error page
        fastcgi_intercept_errors off;

        ### FCGI-Cacheing: Enable the cacheing mechanism
#        fastcgi_cache_valid     200         30h;
#        fastcgi_cache_valid     301         1d;
#        fastcgi_cache           fc_SITE_NAME;
#        fastcgi_cache_bypass    $http_pragma $override_cache;
        ### FCGI-Cacheing: Delegate cacheing decision to App using X-No-Cache header
#        fastcgi_no_cache        $sent_http_x_no_cache $override_cache;

        ### FCGI-Cacheing: Uncomment this for cache debugging via browser
#        add_header              X-Cache $upstream_cache_status;
    }

    ### Show "Not Found" 404 errors in place of "Forbidden" 403 errors, because
    ### forbidden errors allow attackers potential insight into your server's
    ### layout and contents
    error_page                  403 = 404;

    ### Uncomment to enable custom error pages
#    error_page                  403 /error-page/403.html;
#    error_page                  404 /error-page/404.html;
#    error_page                  405 /error-page/405.html;
#    location ^~ /error-page/ {
#        internal;
#        root                    /home/sites/SITE_NAME/pub;
#    }

    ### Error pages must have properly set http headers!
    error_page                  500 501 504 https://temp.raptus.com/overload/;
    error_page                  502 503 https://temp.raptus.com/maintenance/;

    ### Uncomment to enable maintenance mode
#    if ($not-our-ips) {
#        return                  503;
#    }

}

# eof
