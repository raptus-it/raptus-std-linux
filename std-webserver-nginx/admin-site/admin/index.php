<html>
<head>
    <title><?= $_SERVER["SERVER_NAME"] ?></title>
    <link href="/admin/style.css" type="text/css" rel="stylesheet">
</head>

<body>
        <p>
        <h1>Server <?= $_SERVER["SERVER_NAME"] ?></h1>
        <hr>
        <p>
        <h3>Management services for this server</h3>
        <p>
        
        <ul>
          <li><a href="/admin/phpmyadmin">phpMyAdmin</a></li>
          <li><a href="/admin/phpinfo.php">PHP Info</a></li>
        </ul>

        <hr>
        <p>
        <? include("sitelist.inc"); ?>
        </p>
        <hr>
        <h3>Server/Client Information</h3>
        <table>
            <tr><td>REMOTE_ADDR</td><td><?= $_SERVER["REMOTE_ADDR"] ?></td></tr>
            <tr><td>REMOTE_PORT</td><td><?= $_SERVER["REMOTE_PORT"] ?></td></tr>
            <tr><td>SERVER_ADMIN</td><td><?= $_SERVER["SERVER_ADMIN"] ?></td></tr>
            <tr><td>SERVER_NAME</td><td><?= $_SERVER["SERVER_NAME"] ?></td></tr>
            <tr><td>SERVER_ADDR</td><td><?= $_SERVER["SERVER_ADDR"] ?></td></tr>
            <tr><td>SERVER_SOFTWARE</td><td><?= $_SERVER["SERVER_SOFTWARE"] ?></td></tr>
            <tr><td>SERVER_PROTOCOL</td><td><?= $_SERVER["SERVER_PROTOCOL"] ?></td></tr>
            <tr><td>GATEWAY_INTERFACE</td><td><?= $_SERVER["GATEWAY_INTERFACE"] ?></td></tr>
            <tr><td>HTTP_USER_AGENT</td><td><?= $_SERVER["HTTP_USER_AGENT"] ?></td></tr>
        </table>

</body>
</html>

