#!/bin/bash

# only do something if service is active
if ! test "`systemctl is-active mariadb.service`" = "active"
then
    exit
fi

DUMPDIR="/home/backups/mariadb"
DATABASES="`mysql --user=dbadmin --batch --execute 'show databases;'`"
HOUR="`date +%H`"

test -d "$DUMPDIR" || mkdir -p "$DUMPDIR"
for i in $DATABASES
do
  if ! test $i = "Database" && ! test $i = "performance_schema" && ! test $i = "information_schema"
  then
    DUMPFILE="$DUMPDIR/mysql_"$i"_dump_"$HOUR"h.mysql"

    #echo "Dumping $i to $DUMPFILE"
    mysqldump --user=dbadmin --single-transaction --allow-keywords --events $i > $DUMPFILE
    gzip -9 --force --rsyncable $DUMPFILE
  fi
done

# eof
