echo "### Tuning system"

cp $STD_PATH/configs/etc-sysctl.d/rsl-defaults.conf /etc/sysctl.d/rsl-defaults.conf
echo "$STD_PATH/util/network-tuning.sh" >> /etc/rc.local
echo "$STD_PATH/util/rebuild-initramfs-once.sh" >> /etc/rc.local

systemctl disable rpcbind.service >> $LOGFILE 2>&1
